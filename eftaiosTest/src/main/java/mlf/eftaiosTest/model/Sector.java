package mlf.eftaiosTest.model;

import mlf.eftaiosTest.model.exceptions.BadSectorException;


public abstract class Sector {
	
	private final Character col;
	private final Integer row;
	private TypeOfSector type;
	
	public Sector (Character col, Integer row, TypeOfSector type){
		if (row<1||row>14) throw new BadSectorException("error");
		if (col<'A'|| col>'W') throw new BadSectorException("error");
		this.col=col;
		this.row=row;
		this.type=type;
		
		/* da togliere, va messo quando si passa il parametro alla classe sector
		 
		if (typeNumber < 0 || typeNumber > 5)  throw new BadTypeException("error");
		
		if(typeNumber == 0) this.type = TypeOfSector.EMPTY;
		else if(typeNumber == 1) this.type = TypeOfSector.SECURE;
		else if(typeNumber == 2) this.type = TypeOfSector.DANGEROUS;
		else if(typeNumber == 3) this.type = TypeOfSector.HUMAN;
		else if(typeNumber == 4) this.type = TypeOfSector.ALIEN;
		else if(typeNumber == 5) this.type = TypeOfSector.ESCAPE;*/
	}

	public Character getCol() {
		return col;
	}

	public Integer getRow() {
		return row;
	}
	
	public TypeOfSector getType() {
		return type;
	}

	@Override
	public String toString() {
		return "Sector [col=" + col + ", row=" + row + ", type=" + type + "]";
	}

}
