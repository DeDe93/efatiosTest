package mlf.eftaiosTest.model.exceptions;

public class BadMoveException extends GameException {
    public BadMoveException(String message) {
        super(message);
    }
}
