package mlf.eftaiosTest.model.exceptions;

public class BadTypeException extends GameException{
    public BadTypeException(String message) {
        super(message);
    }

}

