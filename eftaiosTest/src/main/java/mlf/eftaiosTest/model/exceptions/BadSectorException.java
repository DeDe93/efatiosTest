package mlf.eftaiosTest.model.exceptions;


public class BadSectorException extends GameException{
    public BadSectorException(String message) {
        super(message);
    }
}
