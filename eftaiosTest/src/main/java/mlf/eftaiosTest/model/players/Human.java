package mlf.eftaiosTest.model.players;

import mlf.eftaiosTest.model.Sector;
import mlf.eftaiosTest.model.TypeOfPlayer;

public class Human implements Player {

	public boolean canMove(Sector from, Sector to) {
		
		return	!hasMovedMoreThanOneVertically(from, to) &&
				!hasMovedMoreThanOneDiagonally(from, to);
		}

	private boolean hasMovedMoreThanOneVertically(Sector from, Sector to) {
		Integer verticalDelta = Math.abs(from.getCol() - to.getCol());
		return verticalDelta > 1;
		}

	private boolean hasMovedMoreThanOneDiagonally(Sector from, Sector to) {
		Integer horizontalDelta = Math.abs(from.getRow() - to.getRow());
		Integer verticalDelta = Math.abs(from.getCol() - to.getCol());
		return horizontalDelta + verticalDelta > 2;
		}

	public TypeOfPlayer getTypeOfPlayer() {
		
		return TypeOfPlayer.HUMAN;
		
	}
	
	
}
