package mlf.eftaiosTest.model.players;

import mlf.eftaiosTest.model.Sector;
import mlf.eftaiosTest.model.TypeOfPlayer;

public interface Player {
	
	boolean canMove (Sector from, Sector to);
	TypeOfPlayer getTypeOfPlayer();		//return type from the enum TypeOfPlayer
	
}
