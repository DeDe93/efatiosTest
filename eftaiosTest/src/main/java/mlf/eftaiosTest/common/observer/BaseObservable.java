package mlf.eftaiosTest.common.observer;

import java.util.HashSet;
import java.util.Set;

public abstract class BaseObservable implements Observable {

    private final Set<Observer> observers;

    public BaseObservable() {
        this.observers = new HashSet<Observer>();
    }

    public void register(Observer obs) {
        observers.add(obs);
    }


    protected final void notify(Event event) {
        for(Observer obs : observers){
            obs.notify(this, event);
        }
    }
}
