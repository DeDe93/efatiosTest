package mlf.eftaiosTest.common.observer;

public interface Observable {
    void register(Observer obs);
}
