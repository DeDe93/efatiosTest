package mlf.eftaiosTest.common.observer;

public interface Observer {
    void notify(Observable source, Event event);
}
