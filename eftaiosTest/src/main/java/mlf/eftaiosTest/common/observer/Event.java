package mlf.eftaiosTest.common.observer;

public final class Event {
    private final String msg;

    public Event(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }
}
